//
//  Contact.swift
//  Address Book
//
//  Created by Umair Gillani on 31/01/2019.
//  Copyright © 2019 Umair Gillani. All rights reserved.
//

import Foundation
import Contacts

class Contact : NSObject {
    
    var name: String?
    var phone: String?
    
    init(cnContact : CNContact) {
        super.init()
        
        self.name = cnContact.givenName + " " + cnContact.familyName
        self.phone = cnContact.phoneNumbers.map() {
            return $0.value.stringValue
            }.joined(separator: " , ")
    }
    
    func getName () -> String {
        return name ?? "N/A"
    }
    
    func getPhone () -> String {
        return phone ?? "N/A"
    }
    
}
