//
//  Globals.swift
//  Address Book
//
//  Created by Umair Gillani on 31/01/2019.
//  Copyright © 2019 Umair Gillani. All rights reserved.
//

import Foundation
import UIKit

let APP_MANAGER = ApplicationManager.create()
let AB_MANAGER = ABManager.create()
let APP_DELEGATE = UIApplication.shared.delegate as! AppDelegate;
