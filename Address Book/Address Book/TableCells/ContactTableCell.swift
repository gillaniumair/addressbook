//
//  ContactTableCell.swift
//  Address Book
//
//  Created by Umair Gillani on 31/01/2019.
//  Copyright © 2019 Umair Gillani. All rights reserved.
//

import UIKit

class ContactTableCell: UITableViewCell {

    @IBOutlet weak var lblname: UILabel!
    @IBOutlet weak var lblPhone: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    var contact : Contact? {
        didSet{
            
            guard let _ = contact else {
                lblname.text = "N/A"
                lblPhone.text = "N/A"
                return
            }
            
            lblname.text = "Name : " + contact!.getName()
            lblPhone.text = "Phone : " + contact!.getPhone()
        }
    }
    
}
