//
//  ContactPresenter.swift
//  Address Book
//
//  Created by Umair Gillani on 31/01/2019.
//  Copyright © 2019 Umair Gillani. All rights reserved.
//

import Foundation

@objc protocol ContactsViewable : class {
    
    @objc optional func permissionGranted ()
    @objc optional func permissionError (error: Error)
    
    @objc optional func contactsLoaded (contacts : [Contact])
    @objc optional func noContactsFound ()
    @objc optional func contactsFailedToLoad (error: Error)
}

class ContactPresenter {

    weak var view : ContactsViewable?
    
    func attach(view : ContactsViewable) {
        self.view = view
    }
    
    func requestPermision () {
        AB_MANAGER.askPermission {
            [weak self] (permision, error) in
            
            guard let bself = self else {
                return
            }
            
            if error == nil {
                bself.view?.permissionGranted?()
            } else {
                bself.view?.permissionError?(error: error!)
            }
            
        }
    }
    
    func loadContacts() {
        AB_MANAGER.fetchContacts {
            [weak self] (flag, contacts) in
            
            guard let bself = self else {
                return
            }
            if flag {
                if contacts!.count > 0 {
                    bself.view?.contactsLoaded?(contacts: contacts!)
                } else {
                    bself.view?.noContactsFound?()
                }
            } else {
                let error = NSError(domain: "999", code: 999, userInfo: [NSLocalizedDescriptionKey: "Something wrong!"])
                bself.view?.contactsFailedToLoad?(error: error as Error)

            }
        }
    }
}
