//
//  ContactsVC.swift
//  Address Book
//
//  Created by Umair Gillani on 31/01/2019.
//  Copyright © 2019 Umair Gillani. All rights reserved.
//

import UIKit

class ContactsVC: UIViewController {
    
    //MARK:- IBOutlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var lblNocontacts: UILabel!
    @IBOutlet weak var loadingView: UIView!
    
    //MARK:- Class Attributes
    var presenter = ContactPresenter()
    var datasource = [Contact]()
    let KCONTACT_CELL_ID = "KCONTACT_CELL_ID"
    
    //MARK:- Class methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.attach(view: self)
        
        tableView.register(UINib(nibName: String(describing: ContactTableCell.self), bundle: Bundle.main), forCellReuseIdentifier: KCONTACT_CELL_ID)
        tableView.estimatedRowHeight = 40
        tableView.rowHeight = UITableView.automaticDimension
        
        presenter.loadContacts()
    }
    
    func reloadTableWithAnimation () {
        UIView.transition(with: tableView,
                          duration: 0.35,
                          options: .allowAnimatedContent,
                          animations: { self.tableView.reloadData() })
    }
    
    //MARK:- IBActions
    @IBAction func btnBackPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension ContactsVC : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datasource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: KCONTACT_CELL_ID) as! ContactTableCell
        cell.contact = datasource[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension ContactsVC : ContactsViewable {
    
    func contactsLoaded(contacts: [Contact]) {
        loadingView.isHidden = true
        datasource.removeAll()
        datasource.append(contentsOf: contacts)
        reloadTableWithAnimation()
    }
    
    func noContactsFound() {
        loadingView.isHidden = false
        lblNocontacts.isHidden = false
        activityIndicator.isHidden = true
    }
    
    func contactsFailedToLoad(error: Error) {
        loadingView.isHidden = false
        lblNocontacts.isHidden = true
        activityIndicator.isHidden = true
        APP_MANAGER.showAlert("Error", title: error.localizedDescription)
    }
}
