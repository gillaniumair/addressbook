//
//  ViewController.swift
//  Address Book
//
//  Created by Umair Gillani on 31/01/2019.
//  Copyright © 2019 Umair Gillani. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    //MARK:- IBOutlets
    @IBOutlet weak var btnList: UIButton!
    
    //MARK:- Class Attributes
    var presenter = ContactPresenter()
    
    //MARK:- Class methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnList.layer.cornerRadius = 20
        presenter.attach(view: self)
    }
    
    func performAction () {
        let access = AB_MANAGER.haveAccessToContacts()
        
        if access.0 == true {
            showContacts()
        } else if access.2 == .denied || access.2 == .restricted {
            APP_MANAGER.showAlert("Error", title: access.1)
        } else {
            //ask permision
            requestPermission()
        }
    }
    
    func requestPermission () {
        presenter.requestPermision()
    }
    
    func showContacts () {
        let contactsVc = ContactsVC()
        contactsVc.modalTransitionStyle = .crossDissolve
        contactsVc.modalPresentationStyle = .overFullScreen
        self.present(contactsVc, animated: true, completion: nil)
    }

    //MARK:- IBActions
    @IBAction func btnListPressed(_ sender: Any) {
        performAction()
    }
}

extension ViewController : ContactsViewable {
    func permissionGranted() {
        showContacts()
    }
    
    func permissionError(error: Error) {
        APP_MANAGER.showAlert("Error", title: error.localizedDescription)
    }
}
