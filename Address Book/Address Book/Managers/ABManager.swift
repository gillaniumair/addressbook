//
//  ABManager.swift
//  Address Book
//
//  Created by Umair Gillani on 31/01/2019.
//  Copyright © 2019 Umair Gillani. All rights reserved.
//

import Foundation
//import AddressBook
import Contacts

class ABManager : NSObject {
    
    static var shared: ABManager?
    
    static func create() -> ABManager {
        if self.shared == nil {
            shared = ABManager()
        }
        
        return shared!
    }
    
    func haveAccessToContacts () -> (Bool,String,CNAuthorizationStatus) {
        
        let abStatus = CNContactStore.authorizationStatus(for: .contacts)
        switch abStatus {
        case .restricted:
            return (false,"You are restricted to access contacts", abStatus)
        case .denied:
            return (false,"You have denied contacts permission, Please enable it from settings", abStatus)
        case .notDetermined:
            return (false,"Permission not asked yet",abStatus)
        default:
            return (true,"Allowed",abStatus)
        }
    }
    
    func askPermission (completion: @escaping ((Bool,Error?) -> Void)) {
        
        CNContactStore().requestAccess(for: .contacts) {
            (granted, error) in
            
            DispatchQueue.main.async {
                if error == nil {
                    completion(true,nil)
                } else {
                    completion(false,error)
                }
            }
        }
    }
    
    func fetchContacts(completion: @escaping (_ success: Bool, _ contacts: [Contact]?) -> Void) {
        
        var contacts = [Contact]()
        
        DispatchQueue.global(qos: .utility).async {
            
            do {
                let contactsFetchRequest = CNContactFetchRequest(keysToFetch: [CNContactGivenNameKey as CNKeyDescriptor, CNContactFamilyNameKey as CNKeyDescriptor, CNContactPhoneNumbersKey as CNKeyDescriptor])
                
                try CNContactStore().enumerateContacts(with: contactsFetchRequest, usingBlock: { (cnContact, error) in
                    if cnContact.areKeysAvailable([CNContactPhoneNumbersKey as CNKeyDescriptor]) {
                        contacts.append(Contact(cnContact: cnContact))
                    }
                    
                })
                
                DispatchQueue.main.async {
                    completion(true, contacts)
                }
                
            } catch {
                
                DispatchQueue.main.async {
                    completion(false, nil)
                }
                
            }
        }
    }
}
