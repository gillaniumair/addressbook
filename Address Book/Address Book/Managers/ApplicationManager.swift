//
//  ApplicationManager.swift
//  Address Book
//
//  Created by Umair Gillani on 31/01/2019.
//  Copyright © 2019 Umair Gillani. All rights reserved.
//

import Foundation
import UIKit

class ApplicationManager : NSObject {
    
    static var shared : ApplicationManager?
    
    static func create() -> ApplicationManager {
        if self.shared == nil {
            shared = ApplicationManager()
        }
        
        return shared!
    }
    
    func showAlert (_ header: String,  title: String) {
        if let topController = APP_DELEGATE.window?.topViewController() {
            let alert = UIAlertController(title: header, message: title, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            topController.present(alert, animated: true, completion: nil)
            
        }
    }
}
